shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_disabled,diffuse_burley,specular_schlick_ggx,world_vertex_coords;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;

uniform float inversion_weight = 1.0;
uniform vec3 inversion_center = vec3(0.0);
uniform float inversion_radius = 1.0;

void vertex() {
	UV=UV*uv1_scale.xy+uv1_offset.xy;
	vec3 world_vert = VERTEX;
	vec3 proj = world_vert-inversion_center;
	float hypotenuse = max( 0.00001, length(proj) );
	float a = inversion_radius / hypotenuse;
	VERTEX = mix( VERTEX, inversion_center + normalize(proj) * a * inversion_radius, inversion_weight );
	NORMAL *= mix( 1.0, -1.0, inversion_weight );
}

void fragment() {
	vec2 base_uv = UV;
	vec4 albedo_tex = texture(texture_albedo,base_uv);
	ALBEDO = albedo.rgb * albedo_tex.rgb;
//	ALBEDO = world_vert;
	METALLIC = metallic;
	ROUGHNESS = roughness;
	SPECULAR = specular;
}
