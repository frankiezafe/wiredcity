tool

extends Spatial

var box_generator = preload( "res://scripts/box_generator.gd" )

export(bool) var generate = false setget _generate
export(Vector3) var cell = Vector3(1,1,1)
export(Vector3) var grid = Vector3(1,1,1) setget _grid

export(Vector3) var translation_variation = Vector3()
export(Vector3) var rotation_variation = Vector3()
export(Vector3) var scale_variation = Vector3(0,0,0)
export(Color) var color_min = Color(1,1,1,1)
export(Color) var color_max = Color(1,1,1,1)

export(bool) var top = true
export(bool) var bottom = true
export(bool) var sides = true
export(float) var split = 0
export(Vector3) var orientation_variation = Vector3()
export(Vector3) var object_variation = Vector3()
export(float) var edge_variation_min = 0.1
export(float) var edge_variation_max = 0.1
export(Vector3) var vertex_variation = Vector3()
export(int) var iteration = 1
export(float, 0,1) var on_floor = 0

export(Vector3) var rotation_anim_min = Vector3()
export(Vector3) var rotation_anim_max = Vector3()

export(Material) var line_material = null
export(bool) var use_unique_material = false
export(bool) var messy_uv = false

var noise_damped = Vector3()
export(float, -5,5) var noise_damping = 0
export(Vector3) var noise_strength = Vector3()

func _grid( v ):
	grid = Vector3( round(v.x), round(v.y), round(v.z) )

func _generate(b):
	generate = false
	if b:
		structure_create()

var generated = false

func structure_create():
	
	print( "structure_create starts" )
	
	while get_child_count() > 0:
		remove_child( get_child(0) )
	
	var mat = load( "res://materials/lines.material" )
	
	if grid.x < 1:
		grid.x = 1
	if grid.y < 1:
		grid.y = 1
	if grid.z < 1:
		grid.z = 1
	
	var total = Vector3(cell.x*(grid.x-1),cell.z*(grid.y-1),cell.x*(grid.z-1))
	var half = total * 0.5
	var offset = -half
	
	for z in range( grid.z ):
		offset.y = -half.y
		for y in range( grid.y ):
			offset.x = -half.x
			for x in range( grid.x ):
				
				var m = MeshInstance.new()
				m.set_script( box_generator )
				
				add_child( m )
				m.set_owner(get_tree().get_edited_scene_root())
				
				m.top = top
				m.bottom = bottom
				m.sides = sides
				m.split = split
				m.orientation_variation = orientation_variation
				m.object_variation = object_variation
				m.edge_variation_min = edge_variation_min
				m.edge_variation_max = edge_variation_max
				m.vertex_variation = vertex_variation
				m.iteration = iteration
				m.on_floor = on_floor
				m.messy_uv = messy_uv
				
				m.box_create()
				m.translation = offset + Vector3( 
					translation_variation.x * (-0.5 + randf()),
					translation_variation.y * (-0.5 + randf()),
					translation_variation.z * (-0.5 + randf())
					)
				
				m.rotation = Vector3( 
					rotation_variation.x * (-0.5 + randf()),
					rotation_variation.y * (-0.5 + randf()),
					rotation_variation.z * (-0.5 + randf()) 
					)
				
				m.scale += Vector3( 
					scale_variation.x * randf(),
					scale_variation.y * randf(),
					scale_variation.z * randf() 
					)
				
				if use_unique_material and line_material != null:
					m.material_override = line_material
				
				else:
					m.material_override = mat.duplicate()
					var diff = color_max - color_min
					m.material_override.albedo_color = color_min + diff * randf()
				
				m.rot_speed = rotation_anim_min + ( rotation_anim_max - rotation_anim_min ) * randf()
				
				offset.x += cell.x
			offset.y += cell.y
		offset.z += cell.x
	
	print( "structure_create ends" )

func _ready():
	pass

func _process(delta):
	
	if not generated:
		generated = true
	
	if messy_uv:
		noise_damped = noise_damped * noise_damping + noise_strength * (1-noise_damping)
		line_material.set_shader_param( "messy", noise_damped )
