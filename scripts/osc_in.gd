tool

extends OSCreceiver

export(bool) var restart = false setget _restart
export(Vector3) var noise_strength_mult = Vector3()
export(Vector3) var noise_strength_mult2 = Vector3()
export(Vector3) var cam_rot_mult = Vector3()
export(Vector3) var cam_trans_mult = Vector3()


func _restart(b):
	start()

func _ready():
	start()
	pass

func _process(delta):

	var structure = get_node("../structure")
	var structure2 = get_node("../structure2")
	var cam = get_node("../cam_pivot")

	while has_waiting_messages():
		
		var msg = get_next_message()
		var addr = msg.address()
		if addr == "/fft/audio1/0":
			structure.noise_strength.y = msg.arg(0) * noise_strength_mult.y
		elif addr == "/fft/audio1/1":
			structure.noise_strength.x = msg.arg(0) * noise_strength_mult.x
		elif addr == "/fft/audio1/2":
			structure.noise_strength.z = msg.arg(0) * noise_strength_mult.z
		
		elif addr == "/fft/audio2/0":
			structure2.noise_strength.y = msg.arg(0) * noise_strength_mult2.y
		elif addr == "/fft/audio2/1":
			structure2.noise_strength.x = msg.arg(0) * noise_strength_mult2.x
		elif addr == "/fft/audio2/2":
			structure2.noise_strength.z = msg.arg(0) * noise_strength_mult2.z
		
		elif addr == "/fft/audio3/0":
			cam.rotation_speed.y = msg.arg(0) * cam_rot_mult.y
		elif addr == "/fft/audio3/1":
			cam.rotation_speed.x = (0.1 - msg.arg(0)) * cam_rot_mult.x
		elif addr == "/fft/audio3/2":
			cam.translation.x = msg.arg(0) * cam_trans_mult.x
		elif addr == "/fft/audio3/3":
			cam.translation.z = msg.arg(0) * cam_trans_mult.z
