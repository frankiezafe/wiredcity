extends OptionButton

onready var mask_mat = get_node('../../cam_pivot/cam/mask').material_override
onready var mask2_mat = get_node('../../cam_pivot/cam/mask2').material_override
onready var mask = get_node('../../cam_pivot/cam/mask')
onready var mask2 = get_node('../../cam_pivot/cam/mask2')

onready var configs = [
	{
		'name': 'none',
		mask : { 'visible': false },
		mask2 : { 'visible': false }
	},
	{
		'name': 'simple',
		mask_mat: {
			'albedo_texture': load( 'res://textures/radial_gradient.png' )
		},
		mask : { 
			'visible': true,
			'size':0.6,
			'subdivision_x' : 1,
			'subdivision_y' : 1,
			'position_deviation' : Vector2(),
			'size_deviation' : Vector2(),
			'repetition' : 1,
			'clear_color': Color(0,0,0,0.05)
		},
		mask2 : { 'visible': false }
	},
	{
		'name': 'square 10x10 (black)',
		mask_mat: {
			'albedo_texture': null
		},
		mask : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 10,
			'subdivision_y' : 10,
			'position_deviation' : Vector2(),
			'size_deviation' : Vector2(),
			'repetition' : 5,
			'clear_color': Color(0,0,0,0.8)
		},
		mask2 : { 'visible': false }
	},
	{
		'name': 'square 10x10 (pink)',
		mask_mat: {
			'albedo_texture': null
		},
		mask2_mat: {
			'albedo_texture': null
		},
		mask : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 10,
			'subdivision_y' : 10,
			'position_deviation' : Vector2(),
			'size_deviation' : Vector2(),
			'repetition' : 4,
			'clear_color': Color(1,0.3,0.41,0.2)
		},
		mask2 : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 21,
			'subdivision_y' : 21,
			'position_deviation' : Vector2(),
			'size_deviation' : Vector2(),
			'repetition' : 10,
			'clear_color': Color(1,0.1,0.31,0.1)
		},
	},
	{
		'name': 'square 10x700 (black)',
		mask_mat: {
			'albedo_texture': null
		},
		mask : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 10,
			'subdivision_y' : 700,
			'position_deviation' : Vector2(.2,0),
			'size_deviation' : Vector2(.1,0.001),
			'repetition' : 300,
			'clear_color': Color(0,0,0,.35)
		},
		mask2 : { 
			'visible': false
		}
	},
	{
		'name': 'square 10x700 (white)',
		mask_mat: {
			'albedo_texture': null
		},
		mask : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 10,
			'subdivision_y' : 700,
			'position_deviation' : Vector2(.2,0),
			'size_deviation' : Vector2(.1,0.001),
			'repetition' : 300,
			'clear_color': Color(1,1,1,.02)
		},
		mask2_mat: {
			'albedo_texture': null
		},
		mask2 : { 
			'visible': true,
			'size':0.6,
			'subdivision_x' : 1,
			'subdivision_y' : 20,
			'position_deviation' : Vector2(0,.02),
			'size_deviation' : Vector2(),
			'repetition' : 5,
			'clear_color': Color(0.9,1,0.88,.01)
		}
	},
	{
		'name': 'grid pattern',
		mask_mat: {
			'albedo_texture': load( 'res://textures/halo.png' )
		},
		mask2_mat: {
			'albedo_texture': load( 'res://textures/halo.png' )
		},
		mask : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 500,
			'subdivision_y' : 3,
			'position_deviation' : Vector2(.58,.05),
			'size_deviation' : Vector2(0,.009),
			'repetition' : 290,
			'clear_color': Color(0,0,0,0.1)
		},
		mask2 : {
			'visible': true,
			'size':0.6,
			'subdivision_x' : 3,
			'subdivision_y' : 500,
			'position_deviation' : Vector2(.05,.58),
			'size_deviation' : Vector2(.009,0),
			'repetition' : 290,
			'clear_color': Color(0,0,0,0.1)
		}
	}
]

func apply_config( o, cfg ):
	if 'visible' in cfg:
		o.visible = cfg.visible
	if 'size' in cfg:
		o.size = cfg.size
	if 'subdivision_x' in cfg:
		o.subdivision_x = cfg.subdivision_x
	if 'subdivision_y' in cfg:
		o.subdivision_y = cfg.subdivision_y
	if 'position_deviation' in cfg:
		o.position_deviation = cfg.position_deviation
	if 'size_deviation' in cfg:
		o.size_deviation = cfg.size_deviation
	if 'repetition' in cfg:
		o.repetition = cfg.repetition
	if 'clear_color' in cfg:
		o.clear_color = cfg.clear_color
	if 'albedo_texture' in cfg:
		o.albedo_texture = cfg.albedo_texture

func _ready():
	for c in configs:
		add_item( c.name )
	_on_configs_item_selected(0)

func _on_configs_item_selected(id):
	if id > -1 and id < len(configs):
		for k in configs[id]:
			if k is String:
				continue
			apply_config(k,configs[id][k])
