tool

extends ImmediateGeometry

export(float,0,10) var size = 2
export(int,0,1000) var subdivision_x = 20
export(int,0,1000) var subdivision_y = 10
export(Vector2) var position_deviation = Vector2()
export(Vector2) var size_deviation = Vector2()
export(int,0,1000) var repetition = 10
export(Color) var clear_color = Color(1,1,1,0.2)

const uv0 = Vector2(0,0)
const uv1 = Vector2(1,0)
const uv2 = Vector2(1,1)
const uv3 = Vector2(0,1)

func _ready():
	pass # Replace with function body.

func add_square( _min, _max ):
	
	set_uv(uv0)
	add_vertex( Vector3(_min.x,_min.y,0) )
	set_uv(uv2)
	add_vertex( Vector3(_max.x,_max.y,0) )
	set_uv(uv1)
	add_vertex( Vector3(_max.x,_min.y,0) )
	
	set_uv(uv0)
	add_vertex( Vector3(_min.x,_min.y,0) )
	set_uv(uv3)
	add_vertex( Vector3(_min.x,_max.y,0) )
	set_uv(uv2)
	add_vertex( Vector3(_max.x,_max.y,0) )

func _process(delta):
	
	if not visible:
		return
	
	var sx = (size*2)
	var sy = (size*2)
	if subdivision_x > 1:
		sx /= (subdivision_x-1)
	if subdivision_y > 1:
		sy /= (subdivision_y-1)
	clear()
	begin(Mesh.PRIMITIVE_TRIANGLES)
	for i in range(repetition):
		
		var ssx = sx + rand_range(-size_deviation.x,size_deviation.x)
		var ssy = sy + rand_range(-size_deviation.y,size_deviation.y)
		
		var xoffset = -size + ( sx - ssx ) * 0.5
		xoffset += sx * int( rand_range(0,subdivision_x) )
		xoffset += rand_range(-position_deviation.x,position_deviation.x)
		
		var yoffset = -size + ( sy - ssy ) * 0.5 
		yoffset += sy * int( rand_range(0,subdivision_y) )
		yoffset += rand_range(-position_deviation.y,position_deviation.y)
		
		add_square( Vector2(xoffset,yoffset), Vector2(xoffset+ssx,yoffset+ssy) )
	end()
	material_override.albedo_color = clear_color
