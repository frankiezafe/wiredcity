tool

extends MeshInstance

export(bool) var generate = false setget _generate
export(bool) var top = true
export(bool) var bottom = true
export(bool) var sides = true
export(float) var split = 0
export(Vector3) var orientation_variation = Vector3()
export(Vector3) var object_variation = Vector3()
export(float) var edge_variation_min = 0.1
export(float) var edge_variation_max = 0.1
export(Vector3) var vertex_variation = Vector3()
export(int) var iteration = 1
export(float, 0,1) var on_floor = 0
export(bool) var messy_uv = false

export(Vector3) var rot_speed = Vector3()

var processed_once = false
var vertices = null
var base = Vector3(1,1,1)
var offset = Vector3()
var vvar = Vector3()

func _generate(b):
	generate = false
	box_create()

func split_vertices():
	if split != 0:
		var tmp = []
		for v in vertices:
			tmp.append( v )
		vertices = []
		for i in range( 0, len( tmp ) / 2 ):
			var a = tmp[i*2]
			var b = tmp[i*2 + 1]
			var mid = b - a
			vertices.append( a )
			vertices.append( a + mid * split )
			vertices.append( b )
			vertices.append( b - mid * split )

func add_line( v0, v1 ):
	var l = ( v1 - v0 ).normalized() * ( edge_variation_min + randf() * (edge_variation_max - edge_variation_min) )
	var p0 = ( offset + v0 + 
		Vector3( 
			vvar.x * (-0.5 + randf()),
			vvar.y * (-0.5 + randf()),
			vvar.z * (-0.5 + randf()) 
			) - l ) 
	var p1 = ( offset + v1 + 
		Vector3( 
			vvar.x * (-0.5 + randf()),
			vvar.y * (-0.5 + randf()),
			vvar.z * (-0.5 + randf()) 
			) + l )
	if on_floor != 0:
		if p0.y < 0:
			p0.y *= 1 - on_floor
		if p1.y < 0:
			p1.y *= 1 - on_floor
		
	vertices.append( p0 )
	vertices.append( p1 )

func box_create():
	
	vertices = []
	for i in range( iteration ):
		base = Vector3( 1,1,1 )
		base += Vector3( 1,1,1 ) * Vector3( 
				(-object_variation.x * 0.5 ) + randf() * object_variation.x,
				(-object_variation.y * 0.5 ) + randf() * object_variation.y,
				(-object_variation.z * 0.5 ) + randf() * object_variation.z
			)
		
		if on_floor > 0:
			offset.y = base.y * 0.5
		else:
			offset.y = 0
		
		var q = Quat()
		q.set_euler( Vector3( 
			(orientation_variation.x * -0.5) + randf() * orientation_variation.x,
			(orientation_variation.y * -0.5) + randf() * orientation_variation.y,
			(orientation_variation.z * -0.5) + randf() * orientation_variation.z ) )
		
		var t = Transform( Basis(q), Vector3() )
		vvar = t.xform( vertex_variation )
		var mmm = t.xform( base * Vector3( -1, -1, -1 ) )
		var pmm = t.xform( base * Vector3( 1, -1, -1 ) )
		var pmp = t.xform( base * Vector3( 1, -1, 1 ) )
		var mmp = t.xform( base * Vector3( -1, -1, 1 ) )
		var mpm = t.xform( base * Vector3( -1, 1, -1 ) )
		var ppm = t.xform( base * Vector3( 1, 1, -1 ) )
		var ppp = t.xform( base * Vector3( 1, 1, 1 ) )
		var mpp = t.xform( base * Vector3( -1, 1, 1 ) )
		
		if top:
			# top
			add_line( mpm, ppm )
			add_line( ppm, ppp )
			add_line( ppp, mpp )
			add_line( mpp, mpm )
			
		if bottom:
			# bottom
			add_line( mmm, pmm )
			add_line( pmm, pmp )
			add_line( pmp, mmp )
			add_line( mmp, mmm )

		if sides:
			add_line( mmm, mpm )
			add_line( pmm, ppm )
			add_line( pmp, ppp)
			add_line( mmp, mpp)
		
		#split_vertices()
	
	var _mesh = Mesh.new()
	var _surf = SurfaceTool.new()
	
	_surf.begin(Mesh.PRIMITIVE_LINES)
	for v in vertices:
		if messy_uv:
			_surf.add_uv( Vector2( 1-randf()*2, 1-randf()*2 ) )
			_surf.add_uv2( Vector2( 1-randf()*2, 1-randf()*2 ) )
		_surf.add_vertex(v)
	_surf.index()
	_surf.commit( _mesh )
	set_mesh( _mesh )
	
	vertices = null
	
	rotation = Vector3()

func _ready():
	box_create()
	pass
	
func _process(delta):
	processed_once = true
	rotation_degrees += rot_speed * delta
