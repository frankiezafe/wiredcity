tool

extends Spatial

export(Vector3) var rotation_speed = Vector3()
export(float,0,1) var messy_strength = 0

func _ready():
	pass # Replace with function body.

func _process(delta):
	rotation_degrees += rotation_speed * delta
	get_node('../objs').get_child(0).material_override.set_shader_param( 'albedo', Color(1,.3 + randf()*0.1,randf()*0.1,.6) )
	get_node('../objs').get_child(0).material_override.set_shader_param( 'messy', Vector3(1-randf()*2,1-randf()*2,1-randf()*2) * messy_strength )
