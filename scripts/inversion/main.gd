extends Spatial

var cam_keys:Array = [0,0,0,0]
var cam_motion:Vector3 = Vector3.ZERO
var cam_rotation:Vector2 = Vector2.ZERO

var inversion_radius:float = 8
var inversion_radius_target:float = 8
var inversion_weight:float = 1

var inverted:Array = []
var pillars:Array = []
var pillar_angle:float = 0

onready var xy_basis:Basis = Basis( Vector3(1,0,0), PI*0.5 )
onready var cam_basis:Basis = $cam_pivot/cam.transform.basis
onready var cam_pos:Vector3 = $cam_pivot/cam.translation
onready var bbox:MeshInstance = $bbox
onready var bbox_mat:SpatialMaterial = $bbox.material_override

func appy_weight() -> void:
	for c in inverted:
		c.material_override.set_shader_param( "inversion_weight", inversion_weight )

func appy_radius() -> void:
	$cam_pivot/center/xy.scale = Vector3.ONE * 2 * inversion_radius
	for c in inverted:
		c.material_override.set_shader_param( "inversion_radius", inversion_radius )

func apply_center() -> void:
	var v3:Vector3 = $cam_pivot.global_transform.origin
	for c in inverted:
		c.material_override.set_shader_param( "inversion_center", v3 )

func _ready() -> void:
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	for c in $inverted.get_children():
		if c is MeshInstance and c.material_override is ShaderMaterial:
			c.mesh.custom_aabb.size = Vector3.ONE * 10000
			inverted.append( c )
			if c.name.begins_with( 'pillar' ):
				pillars.append(c)
			var bb:MeshInstance = bbox.duplicate()
			c.add_child( bb )
			bb.mesh = c.mesh
	
	bbox.visible = false
	appy_weight()
	appy_radius()
	apply_center()

# warning-ignore:unused_argument
func _process(delta) -> void:
	
	pillar_angle += 0.4 * delta
	for i in range( 0, pillars.size() ):
		var p:MeshInstance = pillars[i]
		p.rotation.z = sin( pillar_angle + i * 0.2 ) * PI / 6
	
	var moving:bool = false
	var cam_front:Vector3 = $cam_pivot/cam.global_transform.basis[2] * delta
	var cam_right:Vector3 = $cam_pivot/cam.global_transform.basis[0] * delta
	cam_motion += cam_front * cam_keys[0] - cam_front * cam_keys[2]
	cam_motion += cam_right * cam_keys[1] - cam_right * cam_keys[3]
	if cam_motion.length_squared() > 1:
		cam_motion = cam_motion.normalized()
		moving = true
	elif cam_motion.length_squared() < 1e-5:
		cam_motion *= 0
	else:
		moving = true
	if moving and cam_keys[0]+cam_keys[1]+cam_keys[2]+cam_keys[3] == 0:
		cam_motion *= min( 1, 50 * delta )
	if moving:
		$cam_pivot.translation += cam_motion * 4 * delta
		apply_center()
	
	var mult:float = min( 1, 5*delta )
	$cam_pivot.rotation.y += (cam_rotation.x - $cam_pivot.rotation.y) * mult
	$cam_pivot/center.global_transform.basis = Basis.IDENTITY
	$cam_pivot/cam.translation += ( Basis( cam_basis[0], cam_rotation.y ).xform( cam_pos ) - $cam_pivot/cam.translation ) * mult
	$cam_pivot/cam.look_at( $cam_pivot.global_transform.origin, $cam_pivot/cam.global_transform.basis[1] )
	$cam_pivot/center/xy.global_transform.basis = ( $cam_pivot/cam.global_transform.basis * xy_basis ).scaled( Vector3.ONE * 2 * inversion_radius )
	
	mult = min( 1, 2*delta )
	if inversion_radius != inversion_radius_target:
		inversion_radius += ( inversion_radius_target - inversion_radius ) * mult
		if abs( inversion_radius_target - inversion_radius ) < 1e-4:
			inversion_radius = inversion_radius_target
		appy_radius()

func _input(event) -> void:
	
	if event is InputEventKey:
		match event.scancode:
			KEY_ESCAPE:
				get_tree().quit()
			
			KEY_UP:
				if event.pressed:
					cam_keys[2] = 1
				else:
					cam_keys[2] = 0
			KEY_W:
				if event.pressed:
					cam_keys[2] = 1
				else:
					cam_keys[2] = 0
			
			KEY_RIGHT:
				if event.pressed:
					cam_keys[1] = 1
				else:
					cam_keys[1] = 0
			KEY_D:
				if event.pressed:
					cam_keys[1] = 1
				else:
					cam_keys[1] = 0
			
			KEY_DOWN:
				if event.pressed:
					cam_keys[0] = 1
				else:
					cam_keys[0] = 0
			KEY_S:
				if event.pressed:
					cam_keys[0] = 1
				else:
					cam_keys[0] = 0
			
			KEY_LEFT:
				if event.pressed:
					cam_keys[3] = 1
				else:
					cam_keys[3] = 0
			KEY_A:
				if event.pressed:
					cam_keys[3] = 1
				else:
					cam_keys[3] = 0
			
			KEY_SPACE:
				if event.pressed:
					if bbox_mat.albedo_color.a != 0:
						bbox_mat.albedo_color = Color(1,1,1,0)
					else:
						bbox_mat.albedo_color = Color(1,1,1,0.04)
	
	elif event is InputEventMouseMotion:
		cam_rotation += event.relative * 0.02
	
	elif event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == BUTTON_WHEEL_UP:
				inversion_radius_target += 1
			elif event.button_index == BUTTON_WHEEL_DOWN:
				inversion_radius_target -= 1
				if inversion_radius_target < 1:
					inversion_radius_target = 1
			elif event.button_index == BUTTON_LEFT:
				cam_rotation *= 0
