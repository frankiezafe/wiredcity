tool

extends Spatial

export(float,0,1) var weight:float = 1 setget set_weight
export(float,0,30) var radius:float = 1 setget set_radius 
export(Vector3) var center:Vector3 = Vector3.ZERO setget set_center

func set_weight( f:float ) -> void:
	weight = f
	if is_inside_tree():
		for c in $inverted.get_children():
			if c.material_override is ShaderMaterial:
				c.material_override.set_shader_param( "inversion_weight", weight )

func set_radius( f:float ) -> void:
	radius = f
	if is_inside_tree():
		for c in $inverted.get_children():
			if c.material_override is ShaderMaterial:
				c.material_override.set_shader_param( "inversion_radius", radius )
		$center.scale = Vector3.ONE * radius

func set_center(v:Vector3) -> void:
	center = v
	if is_inside_tree():
		for c in $inverted.get_children():
			if c.material_override is ShaderMaterial:
				c.material_override.set_shader_param( "inversion_center", center )
		$center.global_transform.origin = center

func _ready() -> void:
	set_radius( radius )
	set_center( center )

func _process(delta) -> void:
	pass
